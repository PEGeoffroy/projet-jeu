# Projet groupe

## Inspiration

* Visual novel
* Livre dont vous etes le héros
* Jeux de Rôle
* Point and click
* Cluedo

## Element de gameplay

* Dialogue à choix multiples
* Enigmes
* Test
* Point and click
* Génération aléatoire des personnages et de l'histoire
* Interaction avec l'environnement
* Compte à rebour final
* Compte à rebourg par lieu
* Dialogue trop long PNJ pas content
* Evenement
* Carte

## Contexte

* Policier
* Ambiance sombre, nuit
* Ville, quartier

***

Joueur + PNJ (Personnage Non Joueur)

## PNJ

Homme/Femme

* Gardien (corrompre)

| Requis                                              | Indices laissés | Comportement |
| :-------------------------------------------------- | :-------------- | :----------- |
| Aide pour sortir les poubelles (temps + test force) | Clef            | Boiteux      |
|                                                     | Porte-Clef      |              |

* Sage (enigme)
* Enfant (jeu)

| Requis                                 | Indices laissés | Comportement |
| :------------------------------------- | :-------------- | :----------- |
| Jeu de ballon (test dexterité + temps) | Bonbons         |              |
| Yoyo (test dexterité + temps)          |                 |              |

* Pépé (Qui épie le quartier, pas fiable)
* Mamie (Pas d'indice si pas de gateau, 1 gateau perimé)

| Requis                    | Indices laissés | Comportement |
| :------------------------ | :-------------- | :----------- |
| Raconter histoire (temps) | Cheveux blancs  | Lente        |
| Gateau (risque poison)    | Canne           | pale ???     |
|                           |                 | Tremblement  |

* Homme d'affaire
* Boulanger

| Requis                                       | Indices laissés         | Comportement |
| :------------------------------------------- | :---------------------- | :----------- |
| Achat croissant (argent)                     | Farine (poudre blanche) | Pale         |
| Transport sac farine (temps + test de force) | Tablier                 | Reniflement  |

* Ouvrier
* Eboueur
* Dealer camé

| Requis                | Indices laissés         | Comportement |
| --------------------- | ----------------------- | ------------ |
| Vente drogue (argent) | Drogue (poudre blanche) | Slalom       |
|                       |                         | Reniflement  |
|                       |                         | Trenblement  |

* Etudiant bourré (vomi)

| Requis | Indices laissés                   | Comportement                |
| :----- | :-------------------------------- | :-------------------------- |
| Argent | Vomis                             | Trace de tampon sur la main |
|        | Bouteille                         | Tee-shirt emblème d'école   |
|        | Invitation/tract soirée étudiante |                             |

* Cycliste nocturne (livraison- paiement)
* Dépanneur auto

| Requis                                   | Indices laissés | Comportement       |
| :--------------------------------------- | :-------------- | :----------------- |
| Aide réparation (test dexterité + temps) | Cambouis        | Maculé de cambouis |
|                                          | Boulon          |                    |

* Amant éconduit

| Requis                                                       | Indices laissés   | Comportement |
| :----------------------------------------------------------- | :---------------- | :----------- |
| Écoute (temps + test de charisme) // dialogue chanson d'amour | photo de l'amante | Pleur        |
| Fleur                                                        |                   | Course       |

* (Clown)
* Dame au chat

| Requis                                     | Indices laissés | Comportement   |
| :----------------------------------------- | :-------------- | :------------- |
| Chat dans l'arbre (test dexterité + temps) | Bonbons         | Cheveux blancs |
|                                            |                 | Miaulement     |

* Policier

| Requis | Indices laissés | Comportement |
| :----- | :-------------- | :----------- |
| Donuts | Képi            | Coup de feu  |
|        | Douille         |              |

* Prof de latin
* Fantôme
* Boucher

## Rôle

* Policier Ripou (mauvaise reponse = prison)

## Scénario

> Coupable, meurtre, alibi, fausses pistes

Pourquoi le Pj part enqueter,

* Meurtre
  * Cadavre
  * Scène de crime
  * 3 pistes max (au moins 1 de vraie)
* Braquage (musée, banque)
* Kidnapping/Disparition
* Vol

***

1. Situation initiale
2. Element Perturbateur
3. Péripétie
4. Élément de résolution
5. Situation finale

## Interaction

### Lieu

Le sol est un élément de décor par défaut de tous les lieux.

* Ruelle sombre

| Requis | Elements de décors | Pièges                         |
| ------ | ------------------ | ------------------------------ |
| x      | Porte cochère      | Plaque d'égout (passage musée) |
| x      | Trottoir           |                                |
| x      | Poubelle           |                                |

* Place
* Parc

| Requis                       | Elements de décors     | Pièges          |
| ---------------------------- | ---------------------- | --------------- |
| Escalade (test de dextérité) | Banc                   | Animaux sauvage |
|                              | Buisson                | Puit            |
|                              | Poubelle               |                 |
|                              | Arbre                  |                 |
|                              | Statue (passage musée) |                 |
|                              | Puit (facteur force)   |                 |

* Restaurant
* Mairie

| Requis | Elements de décors | Pièges |
| ------ | ------------------ | ------ |
| Clef   | Comptoir (cache)   | Alarme |
|        | Table              |        |
|        | Prospectus         |        |

* Bar malfamé
* Parking

| Requis | Elements de décors  | Pièges         |
| ------ | ------------------- | -------------- |
| Code   | Voiture             | Flaque d'huile |
|        | Pylône - lampadaire |                |

* Hotel
* Gare
* Garde-meuble
* Église
* Musée

| Requis                  | Elements de décors | Pièges  | Personnage |
| ----------------------- | ------------------ | ------- | ---------- |
| Clef                    | Statue             | Alarme  | Fantôme    |
| Passage secret (indice) | Vitrine            | Gardien |            |

### PNJ - interaction

* Parler
* Observer
* Se cacher

## Passif

> 1 passif

## Caractéristique

> 3 niveaux

* Force
* Dexterité
* Charisme
* Inteligence
* **Observation**

## Objet

* Objet loufoque
* Caillou
* Loupe
* Argent
* Bouteille
* Corde

***

Scénario spécial : decouverte que le coupable c'est le joueur, objectif : suppression indices.